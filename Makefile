all: configuration symlinks appconfig

appname="codea-scm"

configuration:
	[ -f /opt/openresty/nginx/conf/nginx.conf ] || sudo cp ./conf/nginx.conf /opt/openresty/nginx/conf/
	sudo mkdir -p /opt/openresty/nginx/conf/conf.d && sudo chown `whoami` /opt/openresty/nginx/conf/conf.d
	[ -L /opt/openresty/nginx/conf/conf.d/$(appname).conf ] || \
		sudo ln -s `pwd`/conf/conf.d/$(appname).conf /opt/openresty/nginx/conf/conf.d/$(appname).conf
	
symlinks:
	[ -L /opt/$(appname) ] || sudo ln -s `pwd` /opt/$(appname)

appconfig:
	[ -f ./etc/config.lua ] || cp ./etc/config.lua.sample ./etc/config.lua

clean:
	sudo rm -f /opt/$(appname) /opt/openresty/nginx/conf/conf.d/$(appname).conf
	rm -f sockproc
