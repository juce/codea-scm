local util = require("util")


local user = {}

function user.get_user()
    local auth_header = tostring(ngx.req.get_headers().Authorization)
    util.debug("Authorization header: " .. auth_header)
    local username = string.match(auth_header, 'CodeaScmUser ([^%s]+)$')
    if not username then
        return nil, 'Authentication required', 401
    end
    return {
        username = username,
    }
end

return user
