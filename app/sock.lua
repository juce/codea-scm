local shell = require("shell")

local q = ngx.req.get_uri_args().q or ""

local code, out_bytes, err_bytes = shell.execute(q)
if code ~= -1 then
    content = cjson.encode({
        status = "ok",
        output = out_bytes,
        err = err_bytes,
        return_code = code,
    })
else
    content = cjson.encode({
        status = "error",
        messsage = err_bytes,
    })
end

ngx.header.content_type = "application/json"
ngx.header.content_length = #content
ngx.print(content)
