local util = require("util")
local shell = require("shell")
local serializer = require("serializer")


function post()
    local t
    xpcall(function()
        local code, private_key, err = shell.execute('openssl genrsa 2048')
        if code == 0 then
            code, public_key, err = shell.execute('ssh-keygen -y -f /dev/stdin', { data = private_key })
        end
        if code == 0 then
            t = {status = code, public_key = public_key, private_key = private_key}
        else
            t = {status = -1, err = err}
        end
    end,
    function(err)
        t = t or {status = -1, err = err}
    end)

    ngx.header.content_type = 'text/plain'
    ngx.print(serializer.encode(t))
end

        
-- dispatch based on HTTP method
local method = ngx.req.get_method()
local handler = ({get = get, post = post})[method:lower()]
if handler then
    handler()
else
    util.json_error(405, "Unsupported HTTP method for this URI: " .. method)
end
