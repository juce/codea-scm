local os = require("os")
local util = require("util")
local git = require("git")
local user = require("user")
local serializer = require("serializer")


function post()
    local fmt = string.match(ngx.var.uri, '.*[.](%w+)$')

    -- get request body
    ngx.req.read_body()
    local body_data = ngx.req.get_body_data()
    if body_data == nil then
        local filename = ngx.req.get_body_file()
        if filename then
            local inf = io.open(filename)
            body_data = inf:read('*all')
            inf:close()
            util.debug(config.debug and "read from file")
        else
            util.json_error(500, "PROBLEM! did not receive image data")
        end
    else
        util.debug(config.debug and "read from memory")
    end
    util.debug(config.debug and "body data len: " .. #(tostring(body_data)))

    -- get user details and authenticate the user
    local u,err,code = user.get_user()
    if not u then
        local headers = code == 401 and {['WWW-Authenticate'] = 'CodeaScmUser'} or nil
        util.json_error(code or 500, string.format(
            "User-check ERROR. Details: %s", err), nil, headers)
    end

    -- verify correctness of body 
    local doc = (fmt == 'lua') and serializer.decode(body_data) or cjson.decode(body_data)
    local remote = doc.remote
    local tabs, plist, icon, get_diff = doc.tabs, doc.plist, doc.icon, doc.get_diff

    if not remote or not tabs then
        util.json_error(400, "You must specify remote and tabs")
    end

    -- prep files table
    local files = { ["Info.plist"] = plist, ["Icon.png"] = util.hexDecode(icon) }
    for tab_name, tab_content in pairs(tabs) do
        files[string.format('tabs/%s.lua', tab_name)] = tab_content
    end

    u.pk = doc.pk
    git.session(util.new_uuid(), 
        function(wd)
            -- clone repo and checkout master
            local s1, s2 = git.clone_and_checkout(remote, u, "master", wd)

            -- replace files in work directory
            git.replace_files(remote, u, files, wd)

            -- get status
            local s, o = git.get_status(remote, u, wd)
            if not get_diff then
                o = o and "yes" or nil
            end

            local t = {clone_status = s1, checkout_status = s2, status = s, diff = o}
            if fmt == 'lua' then
                ngx.header.content_type = 'text/plain'
                ngx.print(serializer.encode(t))
            else
                ngx.header.content_type = 'application/json'
                ngx.print(cjson.encode(t))
            end
        end,
        function(err)
            util.json_error(500, "PROBLEM: " .. tostring(err))
        end)
end

-- We want the "curl -I" to still work
local head = get

-- dispatch based on HTTP method
local method = ngx.req.get_method()
local handler = ({get = get, post = post})[method:lower()]
if handler then
    handler()
else
    util.json_error(405, "Unsupported HTTP method for this URI: " .. method)
end
