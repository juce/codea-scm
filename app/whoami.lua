local nick = string.match(ngx.req.get_uri_args().nick or "", "[%w_]+") or "incognito"
local public_key = string.match(ngx.req.get_uri_args().public_key or "", "^(ssh[-]%w+ .+)") or "unknown"

ngx.header.content_type = 'text/html'
ngx.say([[<html><head>
<title>Codea-SCM: whoami</title>
<style>
  body { margin-left: 2em; margin-right: 2em; }
  h2,p,a { 
    font-family: sans-serif; 
    word-wrap: break-word; 
  }
  div.keyinfo {
    padding: 0 25px 0 25px;
    border: 1px solid #888;
    background-color: #eee;
    font-size: 1.1em;
    border-radius: 0.5em;
  }
</style>
<body>
<h2>You are: ]] .. nick .. [[</h2>
<div class="keyinfo">
<p id="public_key">]] .. public_key .. [[</p>
</div>
<body></html>
]])
