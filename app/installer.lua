local util = require("util")
local git = require("git")


function get()
    local remote = config.installer_git_remote
    local git_ref = config.installer_git_ref

    local u = {
        username = 'anonymous',
    }

    -- wrap all activities in a "session", so that
    -- we can make sure to clean up the temp directory
    git.session(util.new_uuid(),
        function(wd)
            local status = git.clone_and_checkout(remote, u, git_ref, wd)
            local files = git.get_files(remote, u, wd)

            -- determine order of tabs from Info.plist
            local tab_names = {}
            local buffer_order = string.match(files['Info.plist'], '<key>Buffer Order</key>%s*<array>(.*)</array>')
            for v in string.gmatch(buffer_order, '<string>([^<]+)</string>') do
                tab_names[#tab_names + 1] = v
            end

            local parts = {}
            for i,k in ipairs(tab_names) do
                local v = files[string.format('tabs/%s.lua', k)]
                if v then
                    local marker = v:match('%[([=]*)%[')
                    util.debug('marker: ' .. tostring(marker))
                    marker = marker and (marker .. "=") or ""
                    parts[#parts + 1] = string.format('saveProjectTab("%s",[%s[%s]%s])\n',
                        k, marker, v, marker)
                end
            end
            parts[#parts + 1] = string.format('saveLocalData("version","%s")\n', git_ref)
            local content = table.concat(parts)

            ngx.header.content_type = 'text/plain'
            ngx.header.content_length = #content
            ngx.print(content)

        end, function(err)
            util.json_error(500, "PROBLEM: " .. tostring(err))
        end)
end

-- dispatch based on HTTP method
local method = ngx.req.get_method()
local handler = ({get = get, post = post})[method:lower()]
if handler then
    handler()
else
    util.json_error(405, "Unsupported HTTP method for this URI: " .. method)
end
