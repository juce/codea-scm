-- Utility functions

local util = {}

local shell = require("shell")
local serializer = require("serializer")


-- load and seed uuid generator
local uuid = require("uuid")
ngx.update_time()
uuid.randomseed(ngx.now()*1000)

-- sentinel empty table
util.EMPTY = {}

-- new uuid generator
local hwaddr = config.hwaddr
function util.new_uuid()
    local uuid,_ = uuid.new(hwaddr):gsub('-','')
    return uuid
end

-- debug logger
function util.debug(message)
  return message and ngx.log(ngx.ERR, message)
end

-- debug header smart table
util.debug_header = {}
setmetatable(util.debug_header, {
  __index = function(t, k) return ngx.header[k] end,
  __newindex = function(t, k, v) 
    local debug_on = ngx.req.get_headers().x_debug
    if (tonumber(debug_on) == 1) or (not debug_on and config.debug_headers) then
      ngx.header[k] = v
    end
  end
})

-- simple serializer for tables
function util.dumptable(t)
  if t == nil then
    return string.format("nil")
  end
  local out = {}
  for k,v in pairs(t) do
    out[#out + 1] = tostring(k) .. " = " .. tostring(v)
  end
  return string.format("table{%s}", table.concat(out, ", "))
end

-- count the items in hash part of the table
function hash_size(t)
    local count = 0
    for k,_ in pairs(t) do
        count = count + 1
    end
    return count
end

-- swallows JSON-decode errors, returns nil in that case
function util.safe_json_decode(doc)
    local t
    xpcall(function() 
        t = cjson.decode(doc) 
    end, function(error)
    end)
    return t
end

-- send JSON error response
function util.json_error(status_code, status_msg, info, headers)
  local obj = {
      status = status_msg,
      code = status_code,
  }
  obj.info = info
  ngx.status = status_code
  util.debug('headers = ' .. tostring(headers))
  if headers then
      for name,value in pairs(headers) do
          ngx.header[name] = value
      end
  end
  ngx.header.content_type = 'application/json'
  ngx.say(cjson.encode(obj))
  ngx.exit(ngx.OK)
end

-- iterator to return names of files matching the pattern
function util.findfiles(args)
    local base = args.base or '.'
    local ftype = args.ftype and ('-type ' .. args.ftype) or ''
    local pattern = args.pattern and ("-name '" .. args.pattern .. "'") or ''
    local exclude = args.exclude and ("-not -path '" .. args.exclude .. "'") or ''
    local find_cmd = string.format("find '%s' %s %s %s", base, ftype, pattern, exclude)
    util.debug(find_cmd)
    local exit_code, out, err = shell.execute(find_cmd)
    local paths = {}
    for x in string.gmatch(out or "", '[^\n]+') do
        paths[#paths + 1] = x
    end
    local i = 0
    return function()
        i = i + 1
        return paths[i]
    end
end

function util.serialize_as_lua_code(t)
    return string.format("return %s", util.table_to_lua(t) or 'nil')
end

function util.table_to_lua(t)
    if not t then
        return
    end
    local c = {'{'}
    for k,v in pairs(t) do
        local key = type(k) == 'number' and tostring(k) or string.format('"%s"', k)
        if type(v) == 'table' then
            c[#c + 1] = string.format('[%s]=%s,', key, util.table_to_lua(v))
        elseif type(v) == 'string' then
            local marker = v:match('%[([=]*)%[') 
            marker = marker and (marker .. '=') or ''
            c[#c + 1] = string.format('[%s]=%s,', key,
                string.format('[%s[%s]%s]', marker, v, marker))
        elseif type(v) == 'number' then
            c[#c + 1] = string.format('[%s]=%s,', key, v)
        else
            c[#c + 1] = string.format('[%s]="%s",', key, tostring(v))
        end
    end
    c[#c + 1] = '}\n'
    return table.concat(c, '\n')
end

function util.hexEncode(s)
    return s and string.gsub(s, '.', function(v)
        return string.format("%02x", string.byte(v))
    end) or nil
end

function util.hexDecode(s)
    return s and string.gsub(s, '..', function(v)
        return string.char(tonumber(v, 16))
    end) or nil
end

return util
