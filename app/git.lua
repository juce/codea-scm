-- git utilities module

local os = require("os")
local util = require("util")
local shell = require("shell")

local git = {}

local function dir_for_remote(parent_dir, remote)
    return remote and string.format("%s/%s", parent_dir, remote:gsub('[:/]','_')) or nil
end

function git.session(wd, logic, errback)
    xpcall(function()
        logic(wd)
    end, function(err) 
        if errback then
            errback(err)
        end
    end)
    
    -- cleanup
    if wd then
        shell.execute(string.format("rm -rf '%s/%s'", config.work_home, wd))
    end
end

function git.get_status(remote, u, wd)
    local pdir = wd or dir_for_remote(u.username, remote)
    local cmd = string.format(
        "cd '%s/%s' && git diff --cached --exit-code", config.work_home, pdir)
    local code, out, err = shell.execute(cmd)
    return code, (out ~= "" and out or nil), err
end

function git.commit_and_push(remote, u, comment, wd)
    local author = u.commit_author or string.format('%s at codea-scm <%s@codea-scm.aws.mapote.com>', u.username, u.username)
    local committer_name = 'codea-scm at codea-scm'
    local committer_email = 'codea@scm.aws.mapote.com'
    local pdir = wd or dir_for_remote(u.username, remote)
    local cmd = string.format(
        "cd '%s/%s' && GIT_COMMITTER_NAME='%s' GIT_COMMITTER_EMAIL='%s' git commit --author '%s' -m \"%s\"",
        config.work_home, pdir, committer_name, committer_email, author, comment:gsub('"','\\"'))
    util.debug(config.debug and "cmd #1 = " .. cmd)
    local s1, out1, err1 = shell.execute(cmd)

    cmd = string.format(
        "cd '%s/%s' && GIT_SSH=/opt/codea-scm/scripts/git-ssh /opt/codea-scm/scripts/pipe-to-temp git push origin master",
        config.work_home, pdir)
    util.debug(config.debug and "cmd #2 = " .. cmd)
    local s2, out2, err2 = shell.execute(cmd, { data = u.pk })

    return {
        commit_status = s1, commit_out = s1 ~=0 and out1 or nil, commit_err = s1 ~=0 and err1 or nil,
        push_status = s2, push_out = s2 ~=0 and out2 or nil, push_err = s2 ~=0 and err2 or nil
    }
end

function git.clone_and_checkout(remote, u, ref, wd)
    local pdir = wd or dir_for_remote(u.username, remote)
    local cmd = string.format('rm -rf "%s/%s" && mkdir -p %s', config.work_home, pdir, config.work_home)
    shell.execute(cmd)
    cmd = string.format(
        'GIT_SSH=/opt/codea-scm/scripts/git-ssh /opt/codea-scm/scripts/pipe-to-temp git clone %s "%s/%s"',
        remote, config.work_home, pdir)
    util.debug(config.debug and 'clone-cmd: ' .. cmd)
    local s1, out1, err1 = shell.execute(cmd, {data = u.pk})
    local s2
    if ref and s1 == 0 then
        -- checkout specific ref/branch
        cmd = string.format(
            "cd '%s/%s' && git checkout '%s'", 
            config.work_home, pdir, ref)
        s2, out2, err2 = shell.execute(cmd)
    end
    err1 = s1 ~= 0 and err1 or nil
    err2 = s2 ~= 0 and err2 or nil
    return s1, s2, err1, err2
end

function git.get_log(remote, u, num_entries, wd)
    local pdir = wd or dir_for_remote(u.username, remote)
    local cmd = string.format('cd "%s/%s" && git log -%s --pretty=format:"%%h,%%d"', config.work_home, pdir, num_entries)
    local s, o, e = shell.execute(cmd)
    local t
    if s == 0 then
        t = {}
        for hash, tags in o:gmatch('(%w+)[,]?([^\r\n]*)') do
            tags = tags ~= "" and tags:match('[^%s].*') or nil
            t[#t + 1] = { hash = hash, tags = tags }
        end
    end
    return t, s, e
end

function git.get_files(remote, u, wd)
    local files = {}
    local pdir = wd or dir_for_remote(u.username, remote)
    local base = string.format('%s/%s', config.work_home, pdir)
    for source_file in util.findfiles({base = base, ftype = 'f', exclude = '*/.git*'}) do
        local file_key = source_file:sub(#base+2)
        local f = io.open(source_file)
        files[file_key] = f:read('*all')
        f:close()
    end
    return files
end

function git.replace_files(remote, u, files, wd)
    local pdir = wd or dir_for_remote(u.username, remote)
    local work_dir = string.format('%s/%s', config.work_home, pdir)
    shell.execute(string.format('cd "%s" && git rm -rf *', work_dir))
    for file_path, file_content in pairs(files) do
        -- create dirs, if needed
        local basepath, filename = string.match(file_path, "(.*)/([^/]+)")
        shell.execute(string.format('mkdir -p "%s/%s"', work_dir, basepath))
        -- write out file
        local f = assert(io.open(string.format("%s/%s", work_dir, file_path), 'w'))
        f:write(file_content)
        f:close()
        shell.execute(string.format('cd "%s" && git add "%s"', work_dir, file_path))
    end
end

return git
