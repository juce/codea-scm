# Codea-SCM Web Service

## Introduction

This is a source control proxy service for Codea. It implements the following 
primitives that are translated into corresponding git actions: 
**status, log, pull, commit-and-push.**

## Build

Dependencies:

###1. Openresty ( http://openresty.org )

You will need **openssl** and **pcre** libraries either installed
or source for them downloaded and source dirs provided to the
configure script.

When building, use /opt/openresty as prefix:

    ./configure --prefix=/opt/openresty 
    make
    sudo make install

###2. Sockproc ( https://github.com/juce/sockproc )

To build:

    make

## Run

Start Openresty. 
There is many ways you can run it on your system, depending on what 
flavor of Linux or OSX you are using. One simple way is to just use the helper script:
 
    sudo ./openresty.sh start

Start sockproc under the same user that openresty worker processes
are running. By default, it is "nobody":

    sudo -u nobody ./sockproc /tmp/hello.sock

