#!/bin/bash

prog=/opt/openresty/nginx/sbin/nginx
pidfile=/opt/openresty/nginx/logs/nginx.pid

function start() {
  $prog
}

function stop() {
  $prog -s stop
  retval=$?
}

function status() {
  pid=$(pgrep nginx)
  if [ $? == 0 ]; then
    pid=$(echo $pid | xargs)
    echo "openresty nginx is running ($pid) ..."
    retval=0
  else
    echo "openresty nginx is stopped"
    retval=1
  fi
  return $retval
}

function reload() {
  $prog -s reload
  retval=$?
}

function configtest() {
  $prog -t
  retval=$?
}

retval=0

if [ "$1" == "start" ]; then
  start
elif [ "$1" == "stop" ]; then
  stop
elif [ "$1" == "status" ]; then
  status
elif [ "$1" == "reload" ]; then
  reload
elif [ "$1" == "configtest" ]; then
  configtest
elif [ "$1" == "restart" ]; then
  stop
  sleep 1
  start
else
  echo "Usage $0 {start|stop|restart|reload|configtest|status}"
fi

exit $retval
