#!/bin/bash
bits=${bits:-2048}

name=${1:-key}
openssl genrsa -out ${name}.key $bits
chmod 600 ${name}.key
ssh-keygen -y -f ${name}.key >${name}.pub

#name=${1:-key}
#openssl dsaparam -out dsa_params $bits
#openssl gendsa -out ${name}.key
#chmod 600 ${name}.key
#ssh-keygen -y -f ${name}.key >${name}.pub
